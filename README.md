# Random dogo

Random dogo is a very simple app I created to utilize what I'd just learned about using public APIs. In this github [repo](https://github.com/public-apis/public-apis) I found an API that generates a new dog picture or video every time it's called, so I used it for a quick side project before continuing to learn about AsyncAwait.

## Launching

**** Just open index.html ****

// Dev mode

The app needs to be launched using live-server. 

To install live-server you first need to install [Node](https://nodejs.org/en/).

By installing Node, you also install another program called npm - Node Package Manager. To see if it is correctly installed, run:
```bash
npm -v
```

Now to install live-server you need to run the following command:
```bash 
npm install -g live-server
```

Finally, to host the application you need to cd into the folder in which the app folder is located, and run the following command (Notes-app being the folder name):
```bash
live-server random-dogo
```

const getDog = async () => {
    const response = await fetch('https://random.dog/woof.json')
    if (response.status === 200) {
        const data = await response.json()
        return data.url
    } else { 
        throw new Error('Doggy ran away')
    }
}

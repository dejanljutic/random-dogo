let dogoPic = document.createElement('img')
dogoPic.setAttribute('style', 'max-width:50%; max-height:500px')

let dogoVid = document.createElement('video')
dogoVid.setAttribute('autoplay', true)
dogoVid.setAttribute('controls', true)
dogoVid.setAttribute('style', 'max-width:50%; max-height:500px')

getDog().then((dog) => {
    if (dog.slice(-3) === 'mp4') {
        dogoVid.src = dog
        document.getElementById('dogoDiv').appendChild(dogoVid)
    } else {
        dogoPic.src = dog
        document.getElementById('dogoDiv').appendChild(dogoPic)
    }
}).catch((error) => {
    console.log(`Error: ${error}`)
})

document.getElementById('changeDogo').addEventListener('click', () => {
    window.location.reload(1)
})
